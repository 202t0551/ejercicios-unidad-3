public class subContratado extends Empleado{
   
   private float cantHoras;
   private float precioHora;
   private String nombre;
   private int edad;
   
   public subContratado(){
   
   }
   
   public subContratado(String nombre, int edad, float cantHoras, float precioHora){
      this.nombre = nombre;
      this.edad = edad;
      this.cantHoras = cantHoras;
      this.precioHora = precioHora;
   }

   public float calcularSueldo(float cantHoras, float precioHora){
      float sueldo = 0;
      sueldo = cantHoras * precioHora;
      return sueldo;
   }
   
   
}