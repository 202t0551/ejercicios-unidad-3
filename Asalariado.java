import java.util.Scanner;

public class Asalariado extends Empleado{
   private float sueldo;
   private String nombre;
   private int edad;
   Scanner sc = new Scanner(System.in);

public Asalariado(){
   
}

public Asalariado(String nombre, int edad, float sueldo){
   this.nombre = nombre;
   this.edad = edad;
   this.sueldo = sueldo;
}

public float calcularPago(){
   int meses;
   float resultado;
   System.out.println("----------ASALARIADO----------");
   System.out.println("�Cu�ntos meses trabaj�?");
   meses = sc.nextInt();
   resultado = meses * sueldo;
   return resultado;
}
}